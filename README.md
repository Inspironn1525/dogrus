**CS12320 Mini-Assignment 3: DogsRUs Kennels**
---

## Given problem

Daphne runs the district's Dog Home, looking after stray Dogs and those that can’t be cared for any more by their original owners. 
Daphne re-houses her dogs to supporting families and carers in the surrounding areas.

Daphne has an old barn which she has converted into a number of kennels for the dogs that she houses. 
She keeps a record of all her dogs, together with the original owner (where possible this is the person who donated the dog). 
Old Daphne really worries about her dogs and for each one she carefully maintains details on the kind of food they eat, 
how often they are fed and whether they like bones.

**Your task is to write a Java program that will help Daphne organize her kennels.**

You are already provided with a version that does not support inheritance. 
However, Daphne is asked by her friend Cath to take cats as well as dogs and she has decided to do that. 
Cats are allowed to share a run (can use the same exercise area as other cats). However, she needs to know which cats are able to share. Daphne decided to use this opportunity to reorganise the code so that it will work more flexibly.

Cats and Dogs are both Animals. Some cats can share enclosed run areas (exercise areas), whereas some can’t: they fight. Some dogs need to be taken for a walk whereas some are happy with just the enclosed run provided. Dogs should never share runs. Cats are not interested in bones.
