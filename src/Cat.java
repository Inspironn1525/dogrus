import java.io.PrintWriter;
import java.util.Scanner;

/**
 * To support a cat
 *
 * @author Chris Loftus, Sebastian Pasik
 * @version 1.1 (23th March 2018)
 */
public class Cat extends Animal {

    private boolean isSharing;

    public Cat() {
        this("unknown", "unknown", 1, false);
    }

    public Cat(String name, String favFood, int foodPerDay, boolean isSharing) {
        super(name, favFood, foodPerDay);
        this.isSharing = isSharing;
    }

    /**
     * Reads cat information from the keyboard
     */
    @Override
    public void readKeyboard() {
        System.out
                .println("enter on separate lines: name, favourite food, number of times fed, owner-name, owner-phone, isSharingSpace: ");
        super.readKeyboard();
        isSharing = super.yesOrNo("share space");
    }

    /**
     * Reads in information about the cat from the file
     */
    @Override
    public void load(Scanner infile) {
        super.load(infile);
        isSharing = infile.nextBoolean();
    }
    /**
     * Stores in information about the cat into the file
     */
    @Override
    public void save(PrintWriter pw) {
        pw.println("cat");
        super.save(pw);
        pw.println(isSharing);

    }

    /**
     * A basic implementation to just return all the data in string form
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        sb.append(super.toString())
                .append("Is sharing space? ")
                .append(isSharing)
                .append('\n');
        return sb.toString();
    }

}
