import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * To model a Kennel - a collection of animals
 *
 * @author Chris Loftus, Sebastian Pasik
 * @version 1.2 (23th March 2018)
 */
public class Kennel {
    private String name;
    private ArrayList<Animal> animals;
    private int nextFreeLocation;
    private int capacity;

    /**
     * Creates a kennel with a default size 20
     */
    public Kennel() {
        this(20);
    }

    /**
     * Create a kennel
     *
     * @param maxNoDogs The capacity of the kennel
     */
    public Kennel(int maxNoDogs) {
        nextFreeLocation = 0; // no Dogs in collection at start
        capacity = maxNoDogs;
        animals = new ArrayList<Animal>(capacity); // set up default. This can
        // actually be exceeded
        // when using ArrayList but we
        // won't allow that
        // to happen.
    }

    /**
     * This method sets the value for the name attribute. The purpose of the
     * attribute is: The name of the kennel e.g. "DogsRUs"
     *
     * @param theName
     */
    public void setName(String theName) {
        name = theName;
    }

    /**
     * Set the size of the kennel
     *
     * @param capacity The max animals we can house
     */
    public void setCapacity(int capacity) {
        // This should really check to see if we already have animals
        // in the kennel and reducing the capacity would lead to evictions!
        if (capacity >= this.nextFreeLocation) {
            this.capacity = capacity;
        } else {
            System.err.println("Sorry, You've tried to set less space than there are animals in the kennel");
        }
    }

    /**
     * This method returns the number of animals in a kennel
     *
     * @return int Current number of animals in the kennel
     */
    public int getNumOfAnimals() {
        return nextFreeLocation;
    }

    /**
     * Enables a user to add an Animal to the Kennel
     *
     * @param theAnimal A new dog to home
     */
    public void addAnimal(Animal theAnimal) {
        if (nextFreeLocation >= capacity) {
            System.out.println("Sorry kennel is full - cannot add team");
            return;
        }
        // we add in the position indexed by nextFreeLocation
        // This starts at zero
        animals.add(theAnimal);

        // now increment index ready for next one
        nextFreeLocation = nextFreeLocation + 1;
    }

    /**
     * Enables a user to delete an Animal from the Kennel.
     *
     * @param who The animal to remove
     */
    public void removeAnimal(String who) {
        Animal which = null;
        // Search for the animal by name
        for (Animal a : animals) {
            if (who.equals(a.getName())) {
                which = a;
            }
        }
        if (which != null) {
            animals.remove(which); // Requires that Animal has an equals method
            System.out.println("removed " + who);
            nextFreeLocation = nextFreeLocation - 1;
        } else {
            System.err.println("cannot remove - not in kennel");
        }
    }

    /**
     * @return String showing all the information in the kennel
     */
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        sb.append("Data in Kennel ")
                .append(name)
                .append(" is: \n");
        Collections.sort(animals);
        for (Animal a : animals) {
            sb.append(a.toString())
                    .append("\n");
        }
        return sb.toString();
    }
    /**
     * Returns an array of the inmates in the kennels
     *
     * @return An array of the correct size
     */
    public Animal[] obtainAllInmates() {
        // ENTER CODE HERE (POSSIBLY CHANGE SOME, YOU MAY CHANGE THE SIGNATURE TO DEAL
        // WITH ALL KINDS OF ANIMALS: CATS AND DOGS)
        // SEE Dog.getOriginalOwners METHOD FOR SIMILAR CODE
        Animal[] result = new Animal[animals.size()];
        result = animals.toArray(result);
        return result;
    }


    /**
     * Searches for and returns the inmate, if found
     *
     * @param name The name of the inmate
     * @return The inmate or else null if not found
     */
    public Animal search(String name) {
        // ENTER CODE HERE (POSSIBLY CHANGE SOME, YOU MAY CHANGE THE SIGNATURE TO DEAL
        // WITH ALL KINDS OF ANIMALS: CATS AND DOGS)
        Animal result = null;
        for (Animal a : animals) {
            if (a.getName().equals(name)) {
                result = a;
            }
        }
        return result;
    }

    /**
     * Reads in Kennel information from the file
     *
     * @param infileName The file to read from
     * @throws FileNotFoundException    if file doesn't exist
     * @throws IOException              if some other IO error occurs
     * @throws IllegalArgumentException if infileName is null or empty
     */
    public void load(String infileName) throws IOException {
        // Using try-with-resource. We will cover this in workshop 15, but
        // what it does is to automatically close the file after the try / catch ends.
        // This means we don't have to worry about closing the file.
        try (FileReader fr = new FileReader(infileName);
             BufferedReader br = new BufferedReader(fr);
             Scanner infile = new Scanner(br)) {

            // Use the delimiter pattern so that we don't have to clear end of line
            // characters after doing a nextInt or nextBoolean
            infile.useDelimiter("\r?\n|\r");

            name = infile.next();
            capacity = infile.nextInt();

            int numAnimals = infile.nextInt();
            for (int i = 0; i < numAnimals; i++) {
                Animal a = null;
                String whatAnimal = infile.next();
                switch (whatAnimal) {
                    case "dog":
                        a = new Dog();
                        break;
                    case "cat":
                        a = new Cat();
                        break;
                }
                if (a != null) {
                    a.load(infile);
                    this.addAnimal(a);
                }
            }
        }
    }

    /**
     * Saves the kennel information
     *
     * @param filename The file to save to
     * @throws IOException If some IO error occurs
     */
    public void save(String filename) throws IOException {
        // Again using try-with-resource so that I don't need to close the file explicitly
        try (FileWriter fw = new FileWriter(filename);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter outfile = new PrintWriter(bw);) {

            outfile.println(name);
            outfile.println(capacity);
            outfile.println(this.getNumOfAnimals());
                for (Animal a : animals) {
                    a.save(outfile);
                }
        }
    }
}
