import java.io.PrintWriter;
import java.util.Scanner;

/**
 * To support a dog
 *
 * @author Chris Loftus, Sebastian Pasik
 * @version 1.1 (23th March 2018)
 */
public class Dog extends Animal {

    private boolean likesBones;
    private boolean needWalking;

    /**
     * Default constructor
     */
    public Dog() {

        this("unknown", "unknown", 1, false, false);
    }

    /**
     * Constructor for the dog
     *
     * @param name        The dog's name
     * @param likeBones   Does the dog like bones?
     * @param food        The kind of food it eats
     * @param mealsPerDay Number of feeds per day
     * @param needWalking Does the dog needs to go for a walk?
     **/
    public Dog(String name, String food, int mealsPerDay, boolean likeBones, boolean needWalking) {
        super(name, food, mealsPerDay);
        this.likesBones = likeBones;
        this.needWalking = needWalking;
    }

    /**
     * Reads dog information from the keyboard
     */
    @Override
    public void readKeyboard() {
        System.out
                .println("enter on separate lines: name, favourite food, number of times fed, owner-name, owner-phone,likeBones?, needWalking, ");
        super.readKeyboard();
        likesBones = super.yesOrNo("like bones");
        needWalking = super.yesOrNo("need go for a walk");
    }

    /**
     * Reads in information about the dog from the file
     */
    @Override
    public void load(Scanner infile) {

        super.load(infile);
        likesBones = infile.nextBoolean();
        needWalking = infile.nextBoolean();
    }

    @Override
    public void save(PrintWriter pw) {
        pw.println("dog");
        super.save(pw);
        pw.println(likesBones);
        pw.println(needWalking);
    }

    /**
     * A basic implementation to just return all the data in string form
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        sb.append(super.toString())
                .append("Likes Bones?: ")
                .append(likesBones)
                .append("\nDoes dog go for a walk?: ")
                .append(needWalking)
                .append('\n');
        return sb.toString();
    }

}
